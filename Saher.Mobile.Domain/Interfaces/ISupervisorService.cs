﻿using Saher.Mobile.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Saher.Mobile.Domain.Interfaces
{
    public interface ISupervisorService
    {
        Task UpdateLeaveAsync(long leaveId, bool isApproved);

        Task<List<GuardModel>> GetSupervisorGuardsAsync(string supervisorId);

        Task AddGuardAbsenceAsync(string supervisorId, AbsenceModel model);
    }
}