﻿using Saher.Mobile.Model;
using System.Threading.Tasks;

namespace Saher.Mobile.Domain.Interfaces
{
    public interface IAccountService
    {
        Task<UserInfoModel> AuthenticateUserAsync(string username, string password);

        Task UpdatePasswordAsync(string username, string newPassword);
    }
}