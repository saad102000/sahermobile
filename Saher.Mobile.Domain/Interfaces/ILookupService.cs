﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Saher.Mobile.Data.Db;

namespace Saher.Mobile.Domain.Interfaces
{
    public interface ILookupService
    {
        Task<List<LeaveType>> GetLeaveTypesAsync();
    }

    
}
