﻿using System;

namespace Saher.Mobile.Domain.Helpers
{
    public static class GeoLocationHelper
    {
        public static double CalculateDistanceInMeters((double, double) firstPointLongAndLat,
            (double, double) secondPointLongAndLat)
        {
            var d1 = firstPointLongAndLat.Item2 * (Math.PI / 180.0);
            var num1 = firstPointLongAndLat.Item1 * (Math.PI / 180.0);
            var d2 = secondPointLongAndLat.Item2 * (Math.PI / 180.0);
            var num2 = secondPointLongAndLat.Item1 * (Math.PI / 180.0) - num1;

            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) +
                     Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);

            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }
    }
}