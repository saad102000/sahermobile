﻿using System.Collections.Generic;
using Saher.Mobile.Data.Interfaces;
using Saher.Mobile.Domain.Interfaces;
using Saher.Mobile.Model;
using System.Threading.Tasks;
using Saher.Mobile.Data.Db;

namespace Saher.Mobile.Domain
{
    public class LookupService : ILookupService
    {
        private readonly ILookupRepository _lookupRepository;

        public LookupService(ILookupRepository lookupRepository)
        {
            _lookupRepository = lookupRepository;
        }

        

        public async Task<List<LeaveType>> GetLeaveTypesAsync()
        {
            return await _lookupRepository.GetLeaveTypesAsync();
        }
    }
}