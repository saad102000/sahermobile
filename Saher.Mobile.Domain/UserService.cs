﻿using Microsoft.Extensions.Options;
using Saher.Mobile.Data.Interfaces;
using Saher.Mobile.Domain.Helpers;
using Saher.Mobile.Domain.Interfaces;
using Saher.Mobile.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Saher.Mobile.Domain
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly AppSettings _appSettings;

        public UserService(IUserRepository userRepository, IOptions<AppSettings> settings)
        {
            _userRepository = userRepository;
            _appSettings = settings.Value;
        }

        public async Task<List<AttendanceModel>> GetUserAttendanceAsync(string userId, DateTime? from, DateTime? to)
        {
            return await _userRepository.GetUserAttendanceAsync(userId, from, to);
        }

        public async Task<List<LeaveResponseModel>> GetUserLeavesAsync(string userId, DateTime? from, DateTime? to)
        {
            return await _userRepository.GetUserLeavesAsync(userId, from, to);
        }

        public async Task<long> SubmitLeaveAsync(string userId, LeaveModel model)
        {
            return await _userRepository.SubmitLeaveAsync(userId, model);
        }

        public async Task<LeaveResponseModel> GetLeaveDetailsAsync(string userId, int leaveId)
        {
            return await _userRepository.GetLeaveDetailsAsync(userId, leaveId);
        }

        public async Task CheckInAsync(string userId, CheckInCheckOutModel model)
        {
            (double?, double?) userRegionLocation = await _userRepository.GetCurrentUserRegionLocationAsync(userId);

            if (userRegionLocation.Item1.HasValue && userRegionLocation.Item2.HasValue)
                ValidateUserLocation((userRegionLocation.Item1.Value, userRegionLocation.Item2.Value), (model.Longitude, model.Latitude));

            await _userRepository.CheckInAsync(userId, model);
        }

        public async Task CheckOutAsync(string userId, CheckInCheckOutModel model)
        {
            (double?, double?) userRegionLocation = await _userRepository.GetCurrentUserRegionLocationAsync(userId);

            if (userRegionLocation.Item1.HasValue && userRegionLocation.Item2.HasValue)
                ValidateUserLocation((userRegionLocation.Item1.Value, userRegionLocation.Item2.Value), (model.Longitude, model.Latitude));

            await _userRepository.CheckOutAsync(userId, model);
        }

        public async Task<AttendanceDetailsModel> GetAttendanceDetailsAsync(string userId, long attendanceId)
        {
            return await _userRepository.GetAttendanceDetailsAsync(userId, attendanceId);
        }

        private void ValidateUserLocation((double, double) firstPointLongAndLat,
            (double, double) secondPointLongAndLat)
        {
            var distanceInMeters = GeoLocationHelper.CalculateDistanceInMeters(firstPointLongAndLat,
                secondPointLongAndLat);

            if (distanceInMeters <= _appSettings.MaxAllowedAttendanceDistanceInMeters) return;

            var errorMessage =
                "You are not near the accurate location, please get closer to the correct location and try again.";

            if (!string.IsNullOrWhiteSpace(_appSettings.AttendanceIncorrectLocationError))
                errorMessage = _appSettings.AttendanceIncorrectLocationError;

            throw new InvalidOperationException(errorMessage);
        }
    }
}