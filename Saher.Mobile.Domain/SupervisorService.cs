﻿using Saher.Mobile.Data.Interfaces;
using Saher.Mobile.Domain.Interfaces;
using Saher.Mobile.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Saher.Mobile.Domain
{
    public class SupervisorService : ISupervisorService
    {
        private readonly ISupervisorRepository _supervisorRepository;

        public SupervisorService(ISupervisorRepository supervisorRepository)
        {
            _supervisorRepository = supervisorRepository;
        }

        public async Task UpdateLeaveAsync(long id, bool isApproved)
        {
            await _supervisorRepository.UpdateLeaveAsync(id, isApproved);
        }

        public async Task<List<GuardModel>> GetSupervisorGuardsAsync(string supervisorId)
        {
            return await _supervisorRepository.GetSupervisorGuardsAsync(supervisorId);
        }

        public async Task AddGuardAbsenceAsync(string supervisorId, AbsenceModel model)
        {
            await _supervisorRepository.AddGuardAbsenceAsync(supervisorId, model);
        }
    }
}