﻿using Saher.Mobile.Data.Interfaces;
using Saher.Mobile.Domain.Interfaces;
using Saher.Mobile.Model;
using System.Threading.Tasks;

namespace Saher.Mobile.Domain
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;

        public AccountService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public async Task<UserInfoModel> AuthenticateUserAsync(string username, string password)
        {
            return await _accountRepository.AuthenticateUserAsync(username, password);
        }

        public async Task UpdatePasswordAsync(string username, string newPassword)
        {
            await _accountRepository.UpdatePasswordAsync(username, newPassword);
        }
    }
}