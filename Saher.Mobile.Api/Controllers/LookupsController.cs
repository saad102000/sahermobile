﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Saher.Mobile.Data.Db;
using Saher.Mobile.Domain.Interfaces;
using Saher.Mobile.Model;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Saher.Mobile.Api.Controllers
{
    [Route("api/lookups")]
    [ApiController]
    [ProducesResponseType(typeof(ValidationErrorResponse), (int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType((int)HttpStatusCode.Forbidden)]
    public class LookupsController : ControllerBase
    {
        private readonly ILookupService _lookupService;

        public LookupsController(ILookupService lookupService)
        {
            _lookupService = lookupService;
        }

        /// <summary>
        /// Get leave types list
        /// </summary>

        /// <returns>list of leave types</returns>
        [HttpGet]
        [Route("leave-types")]
        [ProducesResponseType(typeof(List<LeaveType>), (int)HttpStatusCode.OK)]
        [AllowAnonymous]
        public async Task<IActionResult> GetLeaveTypesAsync()
        {
            var leaveTypes = await _lookupService.GetLeaveTypesAsync();

            return Ok(leaveTypes);
        }
    }
}