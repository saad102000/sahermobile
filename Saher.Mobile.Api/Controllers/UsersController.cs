﻿using Microsoft.AspNetCore.Mvc;
using Saher.Mobile.Api.Extensions;
using Saher.Mobile.Api.Filters;
using Saher.Mobile.Domain.Interfaces;
using Saher.Mobile.Model;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Saher.Mobile.Api.Controllers
{
    [Route("api/users")]
    [ApiController]
    [ProducesResponseType(typeof(ValidationErrorResponse), (int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType((int)HttpStatusCode.Forbidden)]
    [MultiplePoliciesAuthorize("Supervisor,Guard")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Gets user attendance records between a date range
        /// </summary>
        /// <param name="userId">Guard or Supervisor Id </param>
        /// <param name="from"> date from </param>
        /// <param name="to">date to</param>
        /// <returns>user's attendance records between date range </returns>
        [HttpGet("{userId}/attendance")]
        [ProducesResponseType(typeof(List<AttendanceModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetUserAttendanceAsync(string userId, DateTime? from, DateTime? to)
        {
            if (User.IsInRole("Gaurd"))
                userId = User.Identity.GetUserId();

            var attendanceList = await _userService.GetUserAttendanceAsync(userId, from, to);
            return Ok(attendanceList);
        }

        /// <summary>
        /// Gets attendance details mainly used by the web app
        /// </summary>
        /// <param name="userId"> User's Id </param>
        /// <param name="attendanceId"> attendance record Id</param>
        /// <returns>Attendance record including gps location and face image</returns>
        [HttpGet("{userId}/attendance/{attendanceId}")]
        [ProducesResponseType(typeof(AttendanceDetailsModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAttendanceDetailsAsync(string userId, long attendanceId)
        {
            if (User.IsInRole("Gaurd"))
                userId = User.Identity.GetUserId();

            AttendanceDetailsModel attendanceDetails = await _userService.GetAttendanceDetailsAsync(userId, attendanceId);
            return Ok(attendanceDetails);
        }

        /// <summary>
        /// Check in a user
        /// </summary>
        /// <param name="userId"> user id</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("{userId}/attendance/check-in")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> CheckInAsync(string userId, CheckInCheckOutModel model)
        {
            if (User.IsInRole("Gaurd"))
                userId = User.Identity.GetUserId();

            try
            {
                await _userService.CheckInAsync(userId, model);
            }
            catch (InvalidOperationException ex)
            {
                var result = new ValidationErrorResponse
                {
                    Message = ex.Message
                };
                return Conflict(result);
            }

            return Ok();
        }

        /// <summary>
        /// Check out a user
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("{userId}/attendance/check-out")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> CheckOutAsync(string userId, CheckInCheckOutModel model)
        {
            if (User.IsInRole("Gaurd"))
                userId = User.Identity.GetUserId();
            try
            {
                await _userService.CheckOutAsync(userId, model);
            }
            catch (InvalidOperationException ex)
            {
                var result = new ValidationErrorResponse
                {
                    Message = ex.Message
                };
                return Conflict(result);
            }

            return Ok();
        }

        /// <summary>
        /// Gets all user leaves between a date range
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="from"> Date from</param>
        /// <param name="to"> Date to</param>
        /// <returns></returns>
        [HttpGet("{userId}/leaves")]
        [ProducesResponseType(typeof(List<LeaveResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetUserLeavesAsync(string userId, DateTime? from, DateTime? to)
        {
            if (User.IsInRole("Gaurd"))
                userId = User.Identity.GetUserId();

            List<LeaveResponseModel> leavesList = await _userService.GetUserLeavesAsync(userId, from, to);
            return Ok(leavesList);
        }

        /// <summary>
        /// Submit a new leave request
        /// </summary>
        /// <param name="userId"> User Id</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("{userId}/leaves")]
        [ProducesResponseType(typeof(long), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SubmitLeaveAsync(string userId, LeaveModel model)
        {
            if (User.IsInRole("Gaurd"))
                userId = User.Identity.GetUserId();

            long leaveId = await _userService.SubmitLeaveAsync(userId, model);
            return Ok(leaveId);
        }

        /// <summary>
        /// Gets leave request details
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="leaveId">Leave Id</param>
        /// <returns></returns>
        [HttpGet("{userId}/leaves/{leaveId}")]
        [ProducesResponseType(typeof(LeaveResponseModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetLeaveDetailsAsync(string userId, int leaveId)
        {
            if (User.IsInRole("Gaurd"))
                userId = User.Identity.GetUserId();

            LeaveResponseModel leaveDetails = await _userService.GetLeaveDetailsAsync(userId, leaveId);
            return Ok(leaveDetails);
        }
    }
}