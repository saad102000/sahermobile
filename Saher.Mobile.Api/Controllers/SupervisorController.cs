﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Saher.Mobile.Api.Extensions;
using Saher.Mobile.Domain.Interfaces;
using Saher.Mobile.Model;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Saher.Mobile.Api.Controllers
{
    [Route("api/supervisors")]
    [ApiController]
    [ProducesResponseType(typeof(ValidationErrorResponse), (int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType((int)HttpStatusCode.Forbidden)]
    [Authorize(Roles = "Supervisor")]
    public class SupervisorController : ControllerBase
    {
        private readonly ISupervisorService _supervisorService;

        public SupervisorController(ISupervisorService supervisorService)
        {
            _supervisorService = supervisorService;
        }

        /// <summary>
        /// Approve or reject leave request
        /// </summary>
        /// <param name="leaveId">Leave Id</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("leaves/{leaveId}")]
        public async Task<IActionResult> UpdateLeaveAsync(long leaveId, LeaveUpdateModel model)
        {
            await _supervisorService.UpdateLeaveAsync(leaveId, model.IsApproved);
            return NoContent();
        }

        /// <summary>
        /// Get supervisor guards list
        /// </summary>
        /// <param name="supervisorId"> Supervisor Id</param>
        /// <returns></returns>
        [HttpGet("{supervisorId}/guards")]
        [ProducesResponseType(typeof(List<GuardModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetSupervisorGuardsAsync(string supervisorId)
        {
            List<GuardModel> guardList = await _supervisorService.GetSupervisorGuardsAsync(supervisorId);
            return Ok(guardList);
        }

        /// <summary>
        /// Add guard absence by supervisor (انسحاب)
        /// </summary>
        /// <param name="model">absence data</param>
        /// <returns>User Information and JWT to be used in subsequent requests</returns>
        [HttpPost]
        [Route("absence")]
        [ProducesResponseType(typeof(UserInfoModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> AddGuardAbsenceAsync(AbsenceModel model)
        {
            var supervisorId = User.Identity.GetUserId();

            await _supervisorService.AddGuardAbsenceAsync(supervisorId, model);
            return Ok();
        }
    }
}