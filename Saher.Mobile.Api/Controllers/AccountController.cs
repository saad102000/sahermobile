﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Saher.Mobile.Api.Extensions;
using Saher.Mobile.Api.Filters;
using Saher.Mobile.Api.Helpers;
using Saher.Mobile.Domain.Interfaces;
using Saher.Mobile.Model;
using System.Net;
using System.Threading.Tasks;

namespace Saher.Mobile.Api.Controllers
{
    [Route("api/account")]
    [ApiController]
    [ProducesResponseType(typeof(ValidationErrorResponse), (int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.InternalServerError)]
    [ProducesResponseType((int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType((int)HttpStatusCode.Forbidden)]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly AppSettings _appSettings;

        public AccountController(IAccountService accountService, IOptions<AppSettings> appSettings)
        {
            _accountService = accountService;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Authenticate users using their UserId and Password
        /// </summary>
        /// <param name="model">containing userId and password</param>
        /// <returns>User Information and JWT to be used in subsequent requests</returns>
        [HttpPost]
        [Route("authentication")]
        [ProducesResponseType(typeof(UserInfoModel), (int)HttpStatusCode.OK)]
        [AllowAnonymous]
        public async Task<IActionResult> AuthenticateUserAsync(LoginModel model)
        {
            var userInfo = await _accountService.AuthenticateUserAsync(model.Username, model.Password);

            if (userInfo == null)
                return Unauthorized();

            userInfo.Jwt = JwtHelper.GenerateJwt(userInfo, _appSettings);

            return Ok(userInfo);
        }

        /// <summary>
        /// Update user password
        /// </summary>
        /// <param name="model">containing new password and confirm password</param>
        [HttpPut]
        [Route("password")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [MultiplePoliciesAuthorize("Supervisor,Guard")]
        public async Task<IActionResult> UpdatePasswordAsync(PasswordUpdateModel model)
        {
            await _accountService.UpdatePasswordAsync(User.Identity.GetUserId(), model.Password);

            return NoContent();
        }
    }
}