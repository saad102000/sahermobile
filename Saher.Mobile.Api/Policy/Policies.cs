﻿using Microsoft.AspNetCore.Authorization;

namespace Saher.Mobile.Api.Policy
{
    public class Policies
    {
        public const string Supervisor = "Supervisor";
        public const string Guard = "Guard";

        public static AuthorizationPolicy SupervisorPolicy()
        {
            return new AuthorizationPolicyBuilder().RequireAuthenticatedUser().RequireRole(Supervisor).Build();
        }

        public static AuthorizationPolicy GuardPolicy()
        {
            return new AuthorizationPolicyBuilder().RequireAuthenticatedUser().RequireRole(Guard).Build();
        }
    }
}