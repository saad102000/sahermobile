﻿using Microsoft.AspNetCore.Mvc;

namespace Saher.Mobile.Api.Filters
{
    public class MultiplePoliciesAuthorizeAttribute : TypeFilterAttribute
    {
        public MultiplePoliciesAuthorizeAttribute(string policies, bool isAnd = false) :
            base(typeof(MultiplePoliciesAuthorizeFilter))
        {
            Arguments = new object[] { policies, isAnd };
        }
    }
}