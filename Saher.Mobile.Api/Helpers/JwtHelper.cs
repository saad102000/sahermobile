﻿using Microsoft.IdentityModel.Tokens;
using Saher.Mobile.Model;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Saher.Mobile.Api.Helpers
{
    public static class JwtHelper
    {
        public static string GenerateJwt(UserInfoModel userInfo, AppSettings appSettings)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings.JwtSecret));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userInfo.EmployeeId),
                new Claim("fullName", userInfo.Fullname),
                new Claim("role", userInfo.Role),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };
            var token = new JwtSecurityToken(
                issuer: appSettings.JwtIssuer,
                audience: appSettings.JwtIssuer,
                claims: claims,
                expires: DateTime.Today.AddDays(appSettings.JwtExpiryInDays),
                signingCredentials: credentials
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}