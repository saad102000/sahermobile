﻿using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace Saher.Mobile.Api.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetUserId(this IIdentity identity)
        {
            var claimsIdentity = identity as ClaimsIdentity;

            return claimsIdentity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;
        }

        public static string GetFullName(this IIdentity identity)
        {
            var claimsIdentity = identity as ClaimsIdentity;
            return claimsIdentity.Claims.FirstOrDefault(x => x.Type == "fullName").Value;
        }
    }
}