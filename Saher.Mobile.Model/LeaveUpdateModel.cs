﻿using System.ComponentModel.DataAnnotations;

namespace Saher.Mobile.Model
{
    public class LeaveUpdateModel
    {
        [Required]
        public bool IsApproved { get; set; }
    }
}