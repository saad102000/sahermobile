﻿using Saher.Mobile.Model.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Saher.Mobile.Model
{
    public class LeaveModel
    {
        [Required]
        public LeaveTypeEnum Type { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }
    }
}