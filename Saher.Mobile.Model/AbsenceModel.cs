﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Saher.Mobile.Model
{
    public class AbsenceModel
    {
        [Required]
        public string GuardId { get; set; }

        [Required]
        public DateTime AbsenceDateTime { get; set; }

        [MaxLength(4000)]
        public string Notes { get; set; }
    }
}