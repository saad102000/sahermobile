﻿namespace Saher.Mobile.Model
{
    public class ValidationError
    {
        public string Description { get; set; }
        public string Name { get; set; }
    }
}