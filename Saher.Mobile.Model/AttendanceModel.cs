﻿using System;

namespace Saher.Mobile.Model
{
    public class AttendanceModel
    {
        public long Id { get; set; }
        public DateTime? AttendanceTime { get; set; }
        public bool IsCheckIn { get; set; }
    }
}