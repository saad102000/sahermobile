﻿using System.Collections.Generic;

namespace Saher.Mobile.Model
{
    public class ValidationErrorResponse : ErrorResponse
    {
        public List<ValidationError> ValidationErrors { get; set; }
    }
}