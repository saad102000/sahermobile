﻿namespace Saher.Mobile.Model
{
    public class LeaveResponseModel : LeaveModel
    {
        public long Id { get; set; }
        public bool? IsApproved { get; set; }
    }
}