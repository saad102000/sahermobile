﻿namespace Saher.Mobile.Model.Enums
{
    public enum LeaveTypeEnum
    {
        Sick = 1,
        Casual = 2,
        Annual = 3,
        Unpaid = 4,
        Permission = 5
    }
}