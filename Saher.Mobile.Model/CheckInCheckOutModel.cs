﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Saher.Mobile.Model
{
    public class CheckInCheckOutModel
    {
        private DateTime? _attendanceTime;

        [Required]
        public double Longitude { get; set; }

        [Required]
        public double Latitude { get; set; }

        public DateTime? AttendanceTime
        {
            get => _attendanceTime;
            set => _attendanceTime = value ?? DateTime.Now;
        }

        [Required]
        public string FaceImage { get; set; }
    }
}