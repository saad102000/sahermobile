﻿using System.ComponentModel.DataAnnotations;

namespace Saher.Mobile.Model
{
    public class PasswordUpdateModel
    {
        [Required(ErrorMessage = "password is required")]
        public string Password { get; set; }

        [Required(ErrorMessage = "password is required")]
        [Compare("Password", ErrorMessage = "Passwords don't match")]
        public string PasswordConfirm { get; set; }
    }
}