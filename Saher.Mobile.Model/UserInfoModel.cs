﻿namespace Saher.Mobile.Model
{
    public class UserInfoModel
    {
        public string Fullname { get; set; }
        public string Jwt { get; set; }
        public string EmployeeId { get; set; }
        public string Role { get; set; }
    }
}