﻿namespace Saher.Mobile.Model
{
    public class AppSettings
    {
        public string JwtSecret { get; set; }
        public string JwtIssuer { get; set; }
        public string AttendanceIncorrectLocationError { get; set; }
        public double JwtExpiryInDays { get; set; }
        public int MaxAllowedAttendanceDistanceInMeters { get; set; }
    }
}