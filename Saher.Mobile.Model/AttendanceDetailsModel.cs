﻿using System;

namespace Saher.Mobile.Model
{
    public class AttendanceDetailsModel
    {
        public long Id { get; set; }
        public bool IsCheckIn { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public DateTime? AttendanceTime { get; set; }
        public string FaceImage { get; set; }
    }
}