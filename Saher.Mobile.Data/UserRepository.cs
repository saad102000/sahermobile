﻿using Microsoft.EntityFrameworkCore;
using Saher.Mobile.Data.Db;
using Saher.Mobile.Data.Interfaces;
using Saher.Mobile.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Saher.Mobile.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly SaherMobileContext _dbContext;

        public UserRepository(SaherMobileContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<AttendanceModel>> GetUserAttendanceAsync(string userId, DateTime? from, DateTime? to)
        {
            var predicate = PredicateBuilder.True<Attendance>();
            predicate = predicate.And(a => a.EmployeeNumber == userId);

            if (from == null && to != null)
            {
                predicate = predicate.And(a => a.AttendanceTime < to);
            }
            if (to == null && from != null)
            {
                predicate = predicate.And(a => a.AttendanceTime > from);
            }
            if (from != null && to != null)
            {
                predicate = predicate.And(a => a.AttendanceTime > from);
                predicate = predicate.And(a => a.AttendanceTime < to);
            }

            var result = await _dbContext.Attendance.Where(predicate)
                .OrderBy(a => a.Id).Select(a => new AttendanceModel
                {
                    Id = a.Id,
                    IsCheckIn = a.IsCheckIn,
                    AttendanceTime = a.AttendanceTime
                }).ToListAsync();

            return result;
        }

        public async Task<List<LeaveResponseModel>> GetUserLeavesAsync(string userId, DateTime? from, DateTime? to)
        {
            var predicate = PredicateBuilder.True<Leave>();
            predicate = predicate.And(a => a.EmployeeNumber == userId);

            if (from == null && to != null)
            {
                predicate = predicate.And(a => a.StartDate < to);
            }
            if (to == null && from != null)
            {
                predicate = predicate.And(a => a.StartDate > from);
            }
            if (from != null && to != null)
            {
                predicate = predicate.And(a => a.StartDate > from);
                predicate = predicate.And(a => a.StartDate < to);
            }

            var result = await _dbContext.Leave.Where(predicate)
                .OrderBy(a => a.Id).Select(a => new LeaveResponseModel
                {
                    Id = a.Id,
                    IsApproved = a.IsApproved,
                    StartDate = a.StartDate,
                    EndDate = a.EndDate,
                    Type = (Model.Enums.LeaveTypeEnum)a.Type
                }).ToListAsync();

            return result;
        }

        public async Task<long> SubmitLeaveAsync(string userId, LeaveModel model)
        {
            var result = await _dbContext.Leave.AddAsync(new Leave
            {
                EmployeeNumber = userId,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                Type = (int)model.Type
            });

            await _dbContext.SaveChangesAsync();

            return result.Entity.Id;
        }

        public async Task<LeaveResponseModel> GetLeaveDetailsAsync(string userId, int leaveId)
        {
            var details = await _dbContext.Leave.FirstOrDefaultAsync(l => l.Id == leaveId);
            return new LeaveResponseModel
            {
                StartDate = details.StartDate,
                IsApproved = details.IsApproved,
                Type = (Model.Enums.LeaveTypeEnum)details.Type,
                EndDate = details.EndDate,
                Id = details.Id
            };
        }

        public async Task CheckInAsync(string userId, CheckInCheckOutModel model)
        {
            await AddAttendance(userId, model, true);
        }

        public async Task CheckOutAsync(string userId, CheckInCheckOutModel model)
        {
            await AddAttendance(userId, model);
        }

        public async Task<AttendanceDetailsModel> GetAttendanceDetailsAsync(string userId, long attendanceId)
        {
            var details = await _dbContext.Attendance.FirstOrDefaultAsync(a => a.Id == attendanceId);

            return new AttendanceDetailsModel
            {
                Id = details.Id,
                IsCheckIn = details.IsCheckIn,
                Longitude = details.Longitude,
                FaceImage = details.FaceImage,
                Latitude = details.Latitude,
                AttendanceTime = details.AttendanceTime
            };
        }

        public async Task<(double?, double?)> GetCurrentUserRegionLocationAsync(string userId)
        {
            var result = await _dbContext.Taskeen.FirstOrDefaultAsync(t => t.FkEmployeeId == userId);
            if (result == null) return (null, null);

            var region =
                await _dbContext.ClientRegion.FirstOrDefaultAsync(r =>
                    r.ErpRegionCode == result.FkErpRegionCode);

            return region == null ? (null, null) : (region.Longitude, region.Latitude);
        }

        private async Task AddAttendance(string userId, CheckInCheckOutModel model, bool isCheckIn = false)
        {
            //TODO: add attendance in ERP first
            await _dbContext.Attendance.AddAsync(new Attendance
            {
                AttendanceTime = model.AttendanceTime.Value,
                EmployeeNumber = userId,
                IsCheckIn = isCheckIn,
                FaceImage = model.FaceImage,
                Latitude = model.Latitude,
                Longitude = model.Longitude
            });

            await _dbContext.SaveChangesAsync();
        }
    }
}