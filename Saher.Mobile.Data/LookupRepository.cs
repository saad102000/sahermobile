﻿using Microsoft.EntityFrameworkCore;
using Saher.Mobile.Data.Db;
using Saher.Mobile.Data.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Saher.Mobile.Data
{
    public class LookupRepository : ILookupRepository
    {
        private readonly SaherMobileContext _dbContext;

        public LookupRepository(SaherMobileContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<LeaveType>> GetLeaveTypesAsync()
        {
            return await _dbContext.LeaveType.ToListAsync();
        }
    }
}