﻿using Microsoft.EntityFrameworkCore;
using Saher.Mobile.Data.Db;
using Saher.Mobile.Data.Interfaces;
using Saher.Mobile.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Saher.Mobile.Data
{
    public class SupervisorRepository : ISupervisorRepository
    {
        private readonly SaherMobileContext _dbContext;

        public SupervisorRepository(SaherMobileContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task UpdateLeaveAsync(long id, bool isApproved)
        {
            var leave = await _dbContext.Leave.FirstOrDefaultAsync(l => l.Id == id);

            leave.IsApproved = isApproved;
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<GuardModel>> GetSupervisorGuardsAsync(string supervisorId)
        {
            return await _dbContext.MobileUsers.Where(u => u.ManagerNumber == supervisorId).Select(g => new GuardModel
            {
                Id = g.EmployeeNumber,
                Name = g.Fullname
            }).ToListAsync();
        }

        public async Task AddGuardAbsenceAsync(string supervisorId, AbsenceModel model)
        {
            await _dbContext.Absence.AddAsync(new Absence
            {
                AbsenceDateTime = model.AbsenceDateTime,
                ManagerNumber = supervisorId,
                EmployeeNumber = model.GuardId,
                Notes = model.Notes
            });

            await _dbContext.SaveChangesAsync();
        }
    }
}