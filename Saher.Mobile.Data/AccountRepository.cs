﻿using Microsoft.EntityFrameworkCore;
using Saher.Mobile.Data.Db;
using Saher.Mobile.Data.Interfaces;
using Saher.Mobile.Model;
using System.Threading.Tasks;

namespace Saher.Mobile.Data
{
    public class AccountRepository : IAccountRepository
    {
        private readonly SaherMobileContext _dbContext;

        public AccountRepository(SaherMobileContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<UserInfoModel> AuthenticateUserAsync(string username, string password)
        {
            var user = await _dbContext.MobileUsers
                .FirstOrDefaultAsync(u => u.EmployeeNumber == username && u.Password == password);
            if (user == null) return null;

            return new UserInfoModel
            {
                EmployeeId = user.EmployeeNumber,
                Fullname = user.Fullname,
                Role = user.Role
            };
        }

        public async Task UpdatePasswordAsync(string username, string newPassword)
        {
            var user = await _dbContext.MobileUsers
                .FirstOrDefaultAsync(u => u.EmployeeNumber == username);
            user.Password = newPassword;

            await _dbContext.SaveChangesAsync();
        }
    }
}