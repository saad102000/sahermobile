﻿using Saher.Mobile.Model;
using System.Threading.Tasks;

namespace Saher.Mobile.Data.Interfaces
{
    public interface IAccountRepository
    {
        Task<UserInfoModel> AuthenticateUserAsync(string username, string password);

        Task UpdatePasswordAsync(string username, string newPassword);
    }
}