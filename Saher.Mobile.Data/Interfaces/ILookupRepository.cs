﻿using System.Collections.Generic;
using Saher.Mobile.Model;
using System.Threading.Tasks;
using Saher.Mobile.Data.Db;

namespace Saher.Mobile.Data.Interfaces
{
    public interface ILookupRepository
    {
        Task<List<LeaveType>> GetLeaveTypesAsync();
    }
}