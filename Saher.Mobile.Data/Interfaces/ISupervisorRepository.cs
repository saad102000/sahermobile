﻿using Saher.Mobile.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Saher.Mobile.Data.Interfaces
{
    public interface ISupervisorRepository
    {
        Task UpdateLeaveAsync(long id, bool isApproved);

        Task<List<GuardModel>> GetSupervisorGuardsAsync(string supervisorId);

        Task AddGuardAbsenceAsync(string supervisorId, AbsenceModel model);
    }
}