﻿using Saher.Mobile.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Saher.Mobile.Data.Interfaces
{
    public interface IUserRepository
    {
        public Task<List<AttendanceModel>> GetUserAttendanceAsync(string userId, DateTime? from, DateTime? to);

        public Task<List<LeaveResponseModel>> GetUserLeavesAsync(string userId, DateTime? from, DateTime? to);

        public Task<long> SubmitLeaveAsync(string userId, LeaveModel model);

        Task<LeaveResponseModel> GetLeaveDetailsAsync(string userId, int leaveId);

        Task CheckInAsync(string userId, CheckInCheckOutModel model);

        Task CheckOutAsync(string userId, CheckInCheckOutModel model);

        Task<AttendanceDetailsModel> GetAttendanceDetailsAsync(string userId, long attendanceId);
        Task<(double?, double?)> GetCurrentUserRegionLocationAsync(string userId);
    }
}