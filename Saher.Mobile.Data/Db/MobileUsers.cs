﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class MobileUsers
    {
        public MobileUsers()
        {
            Absence = new HashSet<Absence>();
        }

        public string EmployeeNumber { get; set; }
        public string Password { get; set; }
        public string Fullname { get; set; }
        public string Role { get; set; }
        public string ManagerNumber { get; set; }

        public virtual ICollection<Absence> Absence { get; set; }
    }
}
