﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class Taskeen
    {
        public int Id { get; set; }
        public int? FkContractId { get; set; }
        public string Contractname { get; set; }
        public int? FkItemId { get; set; }
        public string Itemname { get; set; }
        public string FkShiftId { get; set; }
        public string Shiftname { get; set; }
        public string FkEmployeeId { get; set; }
        public string Employeename { get; set; }
        public DateTime? Tstamp { get; set; }
        public string Costcentername { get; set; }
        public string FkErpRegionCode { get; set; }
    }
}
