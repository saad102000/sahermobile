﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class Acc011ga
    {
        public string FlagType { get; set; }
        public string DocId { get; set; }
        public string Counter { get; set; }
        public string TaxId { get; set; }
        public string TaxType { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? AddedTax { get; set; }
        public decimal? SubtractTax { get; set; }
        public string Adescr { get; set; }
        public string Ldescr { get; set; }
        public string ModuleId { get; set; }
        public string CompId { get; set; }
        public string AddDate { get; set; }
        public string LastDate { get; set; }
        public string UserId { get; set; }
        public double? SAutoKey { get; set; }
        public string ReceiptId { get; set; }
        public string ItemId { get; set; }
        public string AddTime { get; set; }
        public string Sent { get; set; }
        public string LastUser { get; set; }
        public string SSerial { get; set; }
        public string RecOwner { get; set; }
        public string IsTemplate { get; set; }
        public string BatchId { get; set; }
        public string StLocId { get; set; }
        public string ItemRowKey { get; set; }
        public string ArDocId { get; set; }
        public string ApDocId { get; set; }
        public string SiteId { get; set; }
        public string CloseLevel { get; set; }
        public string SApproved { get; set; }
    }
}
