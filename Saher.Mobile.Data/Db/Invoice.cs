﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class Invoice
    {
        public Invoice()
        {
            ExtentionInvoices = new HashSet<ExtentionInvoices>();
        }

        public int Id { get; set; }
        public int? FkContractId { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public decimal? InvoiceTotal { get; set; }
        public decimal? InvoiceWithTax { get; set; }
        public decimal? InvoiceWithoutTax { get; set; }
        public DateTime? Tstamp { get; set; }
        public bool? SyncWithErp { get; set; }
        public string ErpinvoiceId { get; set; }
        public string InvoiceDescription { get; set; }
        public string CcId { get; set; }
        public int? NumberOfDays { get; set; }
        public int? TaxId { get; set; }
        public string PrjId { get; set; }

        public virtual Contract FkContract { get; set; }
        public virtual ICollection<ExtentionInvoices> ExtentionInvoices { get; set; }
    }
}
