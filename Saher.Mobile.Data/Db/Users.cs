﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class Users
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int? FkUserTypeId { get; set; }
        public int? EmployeeNumber { get; set; }
        public int? FkBranchId { get; set; }
        public DateTime? Tstamp { get; set; }

        public virtual Branh FkBranch { get; set; }
    }
}
