﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class WorkItem
    {
        public WorkItem()
        {
            ContractDetails = new HashSet<ContractDetails>();
            ContractDetailsTmp = new HashSet<ContractDetailsTmp>();
        }

        public int Id { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public decimal? ItemPrice { get; set; }
        public int? FkWorkItemType { get; set; }
        public bool? RequiredTaskeen { get; set; }
        public DateTime? Tstamp { get; set; }

        public virtual LutWorkItemType FkWorkItemTypeNavigation { get; set; }
        public virtual ICollection<ContractDetails> ContractDetails { get; set; }
        public virtual ICollection<ContractDetailsTmp> ContractDetailsTmp { get; set; }
    }
}
