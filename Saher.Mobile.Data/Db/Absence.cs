﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class Absence
    {
        public long Id { get; set; }
        public string EmployeeNumber { get; set; }
        public DateTime AbsenceDateTime { get; set; }
        public string ManagerNumber { get; set; }
        public string Notes { get; set; }

        public virtual MobileUsers EmployeeNumberNavigation { get; set; }
    }
}
