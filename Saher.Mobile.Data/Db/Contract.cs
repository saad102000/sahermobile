﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class Contract
    {
        public Contract()
        {
            ContractAttachment = new HashSet<ContractAttachment>();
            ContractExtention = new HashSet<ContractExtention>();
            ContractTax = new HashSet<ContractTax>();
            Invoice = new HashSet<Invoice>();
        }

        public int Id { get; set; }
        public string ContractCode { get; set; }
        public string ManualContractCode { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public int? FkBranchId { get; set; }
        public string FkRegionId { get; set; }
        public bool? AutoInvoices { get; set; }
        public string Notes { get; set; }
        public string Constrains { get; set; }
        public DateTime? ContractDate { get; set; }
        public DateTime? ContractStartDt { get; set; }
        public DateTime? ContractEndDt { get; set; }
        public DateTime? MaturityDate { get; set; }
        public DateTime? CreatedAt { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TotalAmountAfterTax { get; set; }
        public string AreaId { get; set; }
        public string PrjName { get; set; }
        public string PrjId { get; set; }
        public string CcName { get; set; }
        public string CcId { get; set; }
        public int? NumberOfMonthes { get; set; }
        public int? FkTaxId { get; set; }
        public string Email { get; set; }
        public int? Status { get; set; }
        public string AttachemtUri { get; set; }

        public virtual ICollection<ContractAttachment> ContractAttachment { get; set; }
        public virtual ICollection<ContractExtention> ContractExtention { get; set; }
        public virtual ICollection<ContractTax> ContractTax { get; set; }
        public virtual ICollection<Invoice> Invoice { get; set; }
    }
}
