﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class LutExtentionType
    {
        public LutExtentionType()
        {
            ContractExtention = new HashSet<ContractExtention>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public string DescriptionAr { get; set; }

        public virtual ICollection<ContractExtention> ContractExtention { get; set; }
    }
}
