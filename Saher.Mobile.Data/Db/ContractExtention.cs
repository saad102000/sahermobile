﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class ContractExtention
    {
        public ContractExtention()
        {
            ExtentionInvoices = new HashSet<ExtentionInvoices>();
        }

        public int Id { get; set; }
        public string ExtentionManualCode { get; set; }
        public int? FkContractId { get; set; }
        public int? FkExtentionType { get; set; }
        public DateTime? StartDt { get; set; }
        public DateTime? EndDt { get; set; }
        public string Description { get; set; }
        public DateTime? Tstamp { get; set; }
        public int? Total { get; set; }
        public decimal? Total2 { get; set; }
        public bool? Erpinvoices { get; set; }

        public virtual Contract FkContract { get; set; }
        public virtual LutExtentionType FkExtentionTypeNavigation { get; set; }
        public virtual ICollection<ExtentionInvoices> ExtentionInvoices { get; set; }
    }
}
