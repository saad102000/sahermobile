﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Saher.Mobile.Data.Db
{
    public partial class SaherMobileContext : DbContext
    {
        public SaherMobileContext()
        {
        }

        public SaherMobileContext(DbContextOptions<SaherMobileContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Absence> Absence { get; set; }
        public virtual DbSet<Acc01187> Acc01187 { get; set; }
        public virtual DbSet<Acc011ga> Acc011ga { get; set; }
        public virtual DbSet<Attendance> Attendance { get; set; }
        public virtual DbSet<Branh> Branh { get; set; }
        public virtual DbSet<ClientRegion> ClientRegion { get; set; }
        public virtual DbSet<Contract> Contract { get; set; }
        public virtual DbSet<ContractAttachment> ContractAttachment { get; set; }
        public virtual DbSet<ContractDetails> ContractDetails { get; set; }
        public virtual DbSet<ContractDetailsTmp> ContractDetailsTmp { get; set; }
        public virtual DbSet<ContractExtention> ContractExtention { get; set; }
        public virtual DbSet<ContractTax> ContractTax { get; set; }
        public virtual DbSet<ErpSyncLog> ErpSyncLog { get; set; }
        public virtual DbSet<ExtentionInvoices> ExtentionInvoices { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<Leave> Leave { get; set; }
        public virtual DbSet<LeaveType> LeaveType { get; set; }
        public virtual DbSet<LutExtentionType> LutExtentionType { get; set; }
        public virtual DbSet<LutWorkItemType> LutWorkItemType { get; set; }
        public virtual DbSet<MobileUsers> MobileUsers { get; set; }
        public virtual DbSet<Substitute> Substitute { get; set; }
        public virtual DbSet<Taskeen> Taskeen { get; set; }
        public virtual DbSet<Tax> Tax { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<WorkItem> WorkItem { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.;Database=SAHEER;User Id=test;password=test;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Absence>(entity =>
            {
                entity.Property(e => e.AbsenceDateTime).HasColumnType("datetime");

                entity.Property(e => e.EmployeeNumber)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ManagerNumber)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Notes).HasMaxLength(4000);

                entity.HasOne(d => d.EmployeeNumberNavigation)
                    .WithMany(p => p.Absence)
                    .HasForeignKey(d => d.EmployeeNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Absence_MobileUsers");
            });

            modelBuilder.Entity<Acc01187>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ACC01187");

                entity.Property(e => e.AccCurrRate).HasColumnName("ACC_CURR_RATE");

                entity.Property(e => e.AddDate)
                    .HasColumnName("ADD_DATE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AddTime)
                    .HasColumnName("ADD_TIME")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Aname)
                    .HasColumnName("ANAME")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Apply)
                    .HasColumnName("APPLY")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ArAcId)
                    .HasColumnName("AR_AC_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AreaId)
                    .HasColumnName("AREA_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Aremarks)
                    .HasColumnName("AREMARKS")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Aterms)
                    .HasColumnName("ATERMS")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BranchId)
                    .HasColumnName("BRANCH_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CardAmnt).HasColumnName("CARD_AMNT");

                entity.Property(e => e.CashType)
                    .HasColumnName("CASH_TYPE")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.CashVal).HasColumnName("CASH_VAL");

                entity.Property(e => e.CashboxId)
                    .HasColumnName("CASHBOX_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CashierId)
                    .HasColumnName("CASHIER_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CcId)
                    .HasColumnName("CC_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ChiqueNoOld)
                    .HasColumnName("CHIQUE_NO_OLD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ChiqueVal).HasColumnName("CHIQUE_VAL");

                entity.Property(e => e.CloseLevel)
                    .HasColumnName("CLOSE_LEVEL")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CmVouId)
                    .HasColumnName("CM_VOU_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CompId)
                    .HasColumnName("COMP_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreateCb)
                    .HasColumnName("CREATE_CB")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDeposit)
                    .HasColumnName("CREATE_DEPOSIT")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreateSo)
                    .HasColumnName("CREATE_SO")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreditInv)
                    .HasColumnName("CREDIT_INV")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CurrId)
                    .HasColumnName("CURR_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CurrRate)
                    .HasColumnName("CURR_RATE")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.DaArCashInv)
                    .HasColumnName("DA_AR_CASH_INV")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaArSalesarea)
                    .HasColumnName("DA_AR_SALESAREA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DaDict0001)
                    .HasColumnName("DA_DICT_0001")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0002)
                    .HasColumnName("DA_DICT_0002")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0003)
                    .HasColumnName("DA_DICT_0003")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0004)
                    .HasColumnName("DA_DICT_0004")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DaDict0007)
                    .HasColumnName("DA_DICT_0007")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0008)
                    .HasColumnName("DA_DICT_0008")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0009)
                    .HasColumnName("DA_DICT_0009")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DaDict0010)
                    .HasColumnName("DA_DICT_0010")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DaDict0011)
                    .HasColumnName("DA_DICT_0011")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0013)
                    .HasColumnName("DA_DICT_0013")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0014)
                    .HasColumnName("DA_DICT_0014")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0015)
                    .HasColumnName("DA_DICT_0015")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0016)
                    .HasColumnName("DA_DICT_0016")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0017)
                    .HasColumnName("DA_DICT_0017")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0018)
                    .HasColumnName("DA_DICT_0018")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0019)
                    .HasColumnName("DA_DICT_0019")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0021)
                    .HasColumnName("DA_DICT_0021")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0022)
                    .HasColumnName("DA_DICT_0022")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0023)
                    .HasColumnName("DA_DICT_0023")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0024)
                    .HasColumnName("DA_DICT_0024")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0025)
                    .HasColumnName("DA_DICT_0025")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DaDict0026)
                    .HasColumnName("DA_DICT_0026")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0027)
                    .HasColumnName("DA_DICT_0027")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0028)
                    .HasColumnName("DA_DICT_0028")
                    .HasColumnType("text")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0029)
                    .HasColumnName("DA_DICT_0029")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DaDict0030)
                    .HasColumnName("DA_DICT_0030")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0045)
                    .HasColumnName("DA_DICT_0045")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0088)
                    .HasColumnName("DA_DICT_0088")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0089)
                    .HasColumnName("DA_DICT_0089")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0091)
                    .HasColumnName("DA_DICT_0091")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0092)
                    .HasColumnName("DA_DICT_0092")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0101)
                    .HasColumnName("DA_DICT_0101")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0102)
                    .HasColumnName("DA_DICT_0102")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0111)
                    .HasColumnName("DA_DICT_0111")
                    .HasColumnType("text")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0112)
                    .HasColumnName("DA_DICT_0112")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0113)
                    .HasColumnName("DA_DICT_0113")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0120)
                    .HasColumnName("DA_DICT_0120")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0121)
                    .HasColumnName("DA_DICT_0121")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0122)
                    .HasColumnName("DA_DICT_0122")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0778)
                    .HasColumnName("DA_DICT_0778")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict0887)
                    .HasColumnName("DA_DICT_0887")
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict1000)
                    .HasColumnName("DA_DICT_1000")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict1111)
                    .HasColumnName("DA_DICT_1111")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict2222)
                    .HasColumnName("DA_DICT_2222")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict4665)
                    .HasColumnName("DA_DICT_4665")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict6606)
                    .HasColumnName("DA_DICT_6606")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaDict8877)
                    .HasColumnName("DA_DICT_8877")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DaEmpId)
                    .HasColumnName("DA_EMP_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DaStStore)
                    .HasColumnName("DA_ST_STORE")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DepositDocId)
                    .HasColumnName("DEPOSIT_DOC_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DiscRatio).HasColumnName("DISC_RATIO");

                entity.Property(e => e.DiscRatio01)
                    .HasColumnName("DISC_RATIO_01")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DiscRatio02)
                    .HasColumnName("DISC_RATIO_02")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DiscVal)
                    .HasColumnName("DISC_VAL")
                    .HasColumnType("money");

                entity.Property(e => e.DiscVal01)
                    .HasColumnName("DISC_VAL_01")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DiscVal02)
                    .HasColumnName("DISC_VAL_02")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.DiscVal2)
                    .HasColumnName("DISC_VAL2")
                    .HasColumnType("money");

                entity.Property(e => e.DocDate)
                    .HasColumnName("DOC_DATE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DocId)
                    .IsRequired()
                    .HasColumnName("DOC_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DocType)
                    .HasColumnName("DOC_TYPE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DueDate)
                    .HasColumnName("DUE_DATE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DueDays).HasColumnName("DUE_DAYS");

                entity.Property(e => e.EntryType)
                    .HasColumnName("ENTRY_TYPE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Expenses).HasColumnName("EXPENSES");

                entity.Property(e => e.FkContractId).HasColumnName("FK_ContractID");

                entity.Property(e => e.FlagType)
                    .IsRequired()
                    .HasColumnName("FLAG_TYPE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.GroupId)
                    .HasColumnName("GROUP_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HasInstal)
                    .HasColumnName("HAS_INSTAL")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Held)
                    .HasColumnName("HELD")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HoldPay)
                    .HasColumnName("HOLD_PAY")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.InsFdate)
                    .HasColumnName("INS_FDATE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.InsPeriod).HasColumnName("INS_PERIOD");

                entity.Property(e => e.InstAmount)
                    .HasColumnName("INST_AMOUNT")
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.InstNo)
                    .HasColumnName("INST_NO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.InstType)
                    .HasColumnName("INST_TYPE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.InvAddTax).HasColumnName("INV_ADD_TAX");

                entity.Property(e => e.InvNetAfterTax).HasColumnName("INV_NET_AFTER_TAX");

                entity.Property(e => e.InvSubTax).HasColumnName("INV_SUB_TAX");

                entity.Property(e => e.IsTemplate)
                    .HasColumnName("IS_TEMPLATE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ItemsAddTax).HasColumnName("ITEMS_ADD_TAX");

                entity.Property(e => e.ItemsSubTax).HasColumnName("ITEMS_SUB_TAX");

                entity.Property(e => e.JvrTransId)
                    .HasColumnName("JVR_TRANS_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.JvrType)
                    .HasColumnName("JVR_TYPE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LastDate)
                    .HasColumnName("LAST_DATE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LastUser)
                    .HasColumnName("LAST_USER")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Lname)
                    .HasColumnName("LNAME")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Lremarks)
                    .HasColumnName("LREMARKS")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Lterms)
                    .HasColumnName("LTERMS")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleId)
                    .HasColumnName("MODULE_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.NetVal)
                    .HasColumnName("NET_VAL")
                    .HasColumnType("money");

                entity.Property(e => e.NetVal2)
                    .HasColumnName("NET_VAL2")
                    .HasColumnType("money");

                entity.Property(e => e.NoOfIns).HasColumnName("NO_OF_INS");

                entity.Property(e => e.OrgInvId)
                    .HasColumnName("ORG_INV_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OtherPayment)
                    .HasColumnName("OTHER_PAYMENT")
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Owner)
                    .HasColumnName("OWNER")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.PayCurrAmount)
                    .HasColumnName("PAY_CURR_AMOUNT")
                    .HasColumnType("money");

                entity.Property(e => e.PayCurrId)
                    .HasColumnName("PAY_CURR_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PayCurrRate).HasColumnName("PAY_CURR_RATE");

                entity.Property(e => e.PayType)
                    .HasColumnName("PAY_TYPE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PayType1)
                    .HasColumnName("PAY_TYPE1")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PayWithOtherCurr)
                    .HasColumnName("PAY_WITH_OTHER_CURR")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PeriodNo)
                    .HasColumnName("PERIOD_NO")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.PnId)
                    .HasColumnName("PN_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Posted)
                    .HasColumnName("POSTED")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.PrjId)
                    .HasColumnName("PRJ_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RecOwner)
                    .HasColumnName("REC_OWNER")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RecurId)
                    .HasColumnName("RECUR_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RefNo)
                    .HasColumnName("REF_NO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RelatedDocId)
                    .HasColumnName("RELATED_DOC_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RetVal)
                    .HasColumnName("RET_VAL")
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.RuleId)
                    .HasColumnName("RULE_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SApproved)
                    .HasColumnName("S_APPROVED")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SSerial)
                    .HasColumnName("S_SERIAL")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Sent)
                    .HasColumnName("SENT")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ShiftId)
                    .HasColumnName("SHIFT_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasColumnName("SITE_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SlLocId)
                    .HasColumnName("SL_LOC_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SmId)
                    .HasColumnName("SM_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SoDate)
                    .HasColumnName("SO_DATE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SoId)
                    .HasColumnName("SO_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.StVouTyp)
                    .HasColumnName("ST_VOU_TYP")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.StoreId)
                    .HasColumnName("STORE_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TermId)
                    .HasColumnName("TERM_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TotPaid)
                    .HasColumnName("TOT_PAID")
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TotalTax).HasColumnName("TOTAL_TAX");

                entity.Property(e => e.TotalVal)
                    .HasColumnName("TOTAL_VAL")
                    .HasColumnType("money");

                entity.Property(e => e.TotalVal2)
                    .HasColumnName("TOTAL_VAL2")
                    .HasColumnType("money");

                entity.Property(e => e.UserId)
                    .HasColumnName("USER_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VDistrId)
                    .HasColumnName("V_DISTR_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VeDate)
                    .HasColumnName("VE_DATE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.VeDocId)
                    .HasColumnName("VE_DOC_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Void)
                    .HasColumnName("VOID")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.VoidPay)
                    .HasColumnName("VOID_PAY")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.VouSign).HasColumnName("VOU_SIGN");
            });

            modelBuilder.Entity<Acc011ga>(entity =>
            {
                entity.HasKey(e => new { e.FlagType, e.DocId, e.Counter, e.SSerial });

                entity.ToTable("ACC011GA");

                entity.Property(e => e.FlagType)
                    .HasColumnName("FLAG_TYPE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DocId)
                    .HasColumnName("DOC_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Counter)
                    .HasColumnName("COUNTER")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('0000')");

                entity.Property(e => e.SSerial)
                    .HasColumnName("S_SERIAL")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AddDate)
                    .HasColumnName("ADD_DATE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AddTime)
                    .HasColumnName("ADD_TIME")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AddedTax)
                    .HasColumnName("ADDED_TAX")
                    .HasColumnType("money");

                entity.Property(e => e.Adescr)
                    .HasColumnName("ADESCR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApDocId)
                    .HasColumnName("AP_DOC_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ArDocId)
                    .HasColumnName("AR_DOC_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.BatchId)
                    .HasColumnName("BATCH_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CloseLevel)
                    .HasColumnName("CLOSE_LEVEL")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.CompId)
                    .HasColumnName("COMP_ID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.IsTemplate)
                    .HasColumnName("IS_TEMPLATE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId)
                    .HasColumnName("ITEM_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ItemRowKey)
                    .HasColumnName("ITEM_ROW_KEY")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.LastDate)
                    .HasColumnName("LAST_DATE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LastUser)
                    .HasColumnName("LAST_USER")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Ldescr)
                    .HasColumnName("LDESCR")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleId)
                    .HasColumnName("MODULE_ID")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.RecOwner)
                    .HasColumnName("REC_OWNER")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiptId)
                    .HasColumnName("RECEIPT_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SApproved)
                    .HasColumnName("S_APPROVED")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.SAutoKey).HasColumnName("S_AUTO_KEY");

                entity.Property(e => e.Sent)
                    .HasColumnName("SENT")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasColumnName("SITE_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.StLocId)
                    .HasColumnName("ST_LOC_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SubtractTax)
                    .HasColumnName("SUBTRACT_TAX")
                    .HasColumnType("money");

                entity.Property(e => e.TaxAmount)
                    .HasColumnName("TAX_AMOUNT")
                    .HasColumnType("money");

                entity.Property(e => e.TaxId)
                    .HasColumnName("TAX_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TaxType)
                    .HasColumnName("TAX_TYPE")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.UserId)
                    .HasColumnName("USER_ID")
                    .HasMaxLength(12)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Attendance>(entity =>
            {
                entity.Property(e => e.AttendanceTime).HasColumnType("datetime");

                entity.Property(e => e.EmployeeNumber)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FaceImage).IsRequired();
            });

            modelBuilder.Entity<Branh>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BranchCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BranchName).HasMaxLength(50);

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientRegion>(entity =>
            {
                entity.ToTable("Client_Region");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ErpClientCode)
                    .HasColumnName("ERP_ClientCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ErpClientName)
                    .HasColumnName("ERP_ClientName")
                    .HasMaxLength(200);

                entity.Property(e => e.ErpProjectCode)
                    .HasColumnName("ERP_ProjectCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ErpProjectName)
                    .HasColumnName("ERP_ProjectName")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ErpRegionCode)
                    .HasColumnName("ERP_RegionCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ErpRegionName)
                    .HasColumnName("ERP_RegionName")
                    .HasMaxLength(200);

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Contract>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AreaId)
                    .HasColumnName("Area_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AttachemtUri)
                    .HasColumnName("attachemtURI")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AutoInvoices).HasColumnName("Auto_Invoices");

                entity.Property(e => e.CcId)
                    .HasColumnName("CC_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CcName)
                    .HasColumnName("CC_name")
                    .HasMaxLength(100);

                entity.Property(e => e.ClientCode)
                    .HasColumnName("Client_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.ClientName)
                    .HasColumnName("Client_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.ContractCode).HasMaxLength(50);

                entity.Property(e => e.ContractDate)
                    .HasColumnName("Contract_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ContractEndDt)
                    .HasColumnName("Contract_End_dt")
                    .HasColumnType("datetime");

                entity.Property(e => e.ContractStartDt)
                    .HasColumnName("Contract_Start_dt")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("Created_At")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FkBranchId).HasColumnName("FK_BranchID");

                entity.Property(e => e.FkRegionId)
                    .HasColumnName("FK_RegionID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FkTaxId).HasColumnName("FK_TAX_ID");

                entity.Property(e => e.ManualContractCode).HasMaxLength(50);

                entity.Property(e => e.MaturityDate)
                    .HasColumnName("maturity_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.NumberOfMonthes).HasColumnName("Number_Of_Monthes");

                entity.Property(e => e.PrjId)
                    .HasColumnName("Prj_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrjName)
                    .HasColumnName("Prj_name")
                    .HasMaxLength(100);

                entity.Property(e => e.TotalAmount).HasColumnType("money");

                entity.Property(e => e.TotalAmountAfterTax).HasColumnType("money");
            });

            modelBuilder.Entity<ContractAttachment>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AttachmentUri)
                    .HasColumnName("AttachmentURI")
                    .HasMaxLength(200);

                entity.Property(e => e.FkContractId).HasColumnName("FK_ContractID");

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.FkContract)
                    .WithMany(p => p.ContractAttachment)
                    .HasForeignKey(d => d.FkContractId)
                    .HasConstraintName("FK_ContractAttachment_Contract");
            });

            modelBuilder.Entity<ContractDetails>(entity =>
            {
                entity.ToTable("Contract_Details");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.CcId)
                    .HasColumnName("CC_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate)
                    .HasColumnName("End_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FkContractId).HasColumnName("FK_ContractId");

                entity.Property(e => e.FkExtid).HasColumnName("FK_EXTID");

                entity.Property(e => e.FkWorkItemId).HasColumnName("FK_WorkItemID");

                entity.Property(e => e.IsConfirmed).HasColumnName("Is_Confirmed");

                entity.Property(e => e.IsRemoved).HasColumnName("is_Removed");

                entity.Property(e => e.NumberOfWorkItem).HasColumnName("Number_Of_WorkItem");

                entity.Property(e => e.PrjId)
                    .HasColumnName("prj_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDt)
                    .HasColumnName("Start_dt")
                    .HasColumnType("datetime");

                entity.Property(e => e.TotalAmount)
                    .HasColumnName("Total_Amount")
                    .HasColumnType("money");

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");

                entity.Property(e => e.WorkItemPrice)
                    .HasColumnName("WorkItem_price")
                    .HasColumnType("money");

                entity.HasOne(d => d.FkWorkItem)
                    .WithMany(p => p.ContractDetails)
                    .HasForeignKey(d => d.FkWorkItemId)
                    .HasConstraintName("FK_Contract_Details_Contract");
            });

            modelBuilder.Entity<ContractDetailsTmp>(entity =>
            {
                entity.ToTable("Contract_Details_TMP");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CcId)
                    .HasColumnName("CC_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate)
                    .HasColumnName("End_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FkWorkItemId).HasColumnName("FK_WorkItemID");

                entity.Property(e => e.NumberOfWorkItem).HasColumnName("Number_Of_WorkItem");

                entity.Property(e => e.PrjId)
                    .HasColumnName("prj_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SessionId)
                    .HasColumnName("session_id")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.StartDt)
                    .HasColumnName("Start_dt")
                    .HasColumnType("datetime");

                entity.Property(e => e.TotalAmount)
                    .HasColumnName("Total_Amount")
                    .HasColumnType("money");

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");

                entity.Property(e => e.WorkItemPrice)
                    .HasColumnName("WorkItem_price")
                    .HasColumnType("money");

                entity.HasOne(d => d.FkWorkItem)
                    .WithMany(p => p.ContractDetailsTmp)
                    .HasForeignKey(d => d.FkWorkItemId)
                    .HasConstraintName("FK_Contract_Details_TMP_WorkItem");
            });

            modelBuilder.Entity<ContractExtention>(entity =>
            {
                entity.ToTable("Contract_Extention");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EndDt)
                    .HasColumnName("End_dt")
                    .HasColumnType("datetime");

                entity.Property(e => e.Erpinvoices).HasColumnName("ERPInvoices");

                entity.Property(e => e.ExtentionManualCode)
                    .HasColumnName("Extention_Manual_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.FkContractId).HasColumnName("FK_ContractID");

                entity.Property(e => e.FkExtentionType).HasColumnName("FK_ExtentionType");

                entity.Property(e => e.StartDt)
                    .HasColumnName("Start_dt")
                    .HasColumnType("datetime");

                entity.Property(e => e.Total2).HasColumnType("money");

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.FkContract)
                    .WithMany(p => p.ContractExtention)
                    .HasForeignKey(d => d.FkContractId)
                    .HasConstraintName("FK_Contract_Extention_Contract");

                entity.HasOne(d => d.FkExtentionTypeNavigation)
                    .WithMany(p => p.ContractExtention)
                    .HasForeignKey(d => d.FkExtentionType)
                    .HasConstraintName("FK_Contract_Extention_LUT_ExtentionType");
            });

            modelBuilder.Entity<ContractTax>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Erptaxid)
                    .HasColumnName("ERPTAXID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FkContractId).HasColumnName("FK_ContractID");

                entity.Property(e => e.FkTaxId).HasColumnName("FK_TaxID");

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.FkContract)
                    .WithMany(p => p.ContractTax)
                    .HasForeignKey(d => d.FkContractId)
                    .HasConstraintName("FK_ContractTax_Contract");

                entity.HasOne(d => d.FkTax)
                    .WithMany(p => p.ContractTax)
                    .HasForeignKey(d => d.FkTaxId)
                    .HasConstraintName("FK_ContractTax_Tax");
            });

            modelBuilder.Entity<ErpSyncLog>(entity =>
            {
                entity.ToTable("ERP_SyncLog");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FkInvoiceId).HasColumnName("FK_InvoiceID");

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<ExtentionInvoices>(entity =>
            {
                entity.ToTable("Extention_Invoices");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AddedAmount)
                    .HasColumnName("added_amount")
                    .HasColumnType("money");

                entity.Property(e => e.AddedAmountWithoutTaxt)
                    .HasColumnName("added_amount_without_taxt")
                    .HasColumnType("money");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("money");

                entity.Property(e => e.AmountWithoutTaxt)
                    .HasColumnName("amount_without_taxt")
                    .HasColumnType("money");

                entity.Property(e => e.FkExtentionId).HasColumnName("FK_ExtentionID");

                entity.Property(e => e.FkInvoiceId).HasColumnName("FK_InvoiceID");

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.FkExtention)
                    .WithMany(p => p.ExtentionInvoices)
                    .HasForeignKey(d => d.FkExtentionId)
                    .HasConstraintName("FK_Extention_Invoices_Contract_Extention");

                entity.HasOne(d => d.FkInvoice)
                    .WithMany(p => p.ExtentionInvoices)
                    .HasForeignKey(d => d.FkInvoiceId)
                    .HasConstraintName("FK_Extention_Invoices_Invoice");
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CcId)
                    .HasColumnName("cc_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ErpinvoiceId)
                    .HasColumnName("ERPInvoiceID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FkContractId).HasColumnName("FK_contractId");

                entity.Property(e => e.InvoiceDate)
                    .HasColumnName("Invoice_Date")
                    .HasColumnType("date");

                entity.Property(e => e.InvoiceDescription).HasMaxLength(500);

                entity.Property(e => e.InvoiceTotal)
                    .HasColumnName("Invoice_Total")
                    .HasColumnType("money");

                entity.Property(e => e.InvoiceWithTax)
                    .HasColumnName("Invoice_with_tax")
                    .HasColumnType("money");

                entity.Property(e => e.InvoiceWithoutTax)
                    .HasColumnName("invoice_without_tax")
                    .HasColumnType("money");

                entity.Property(e => e.NumberOfDays).HasColumnName("number_of_days");

                entity.Property(e => e.PrjId)
                    .HasColumnName("prj_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SyncWithErp).HasColumnName("SyncWithERP");

                entity.Property(e => e.TaxId).HasColumnName("tax_id");

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.FkContract)
                    .WithMany(p => p.Invoice)
                    .HasForeignKey(d => d.FkContractId)
                    .HasConstraintName("FK_Invoice_Contract");
            });

            modelBuilder.Entity<Leave>(entity =>
            {
                entity.Property(e => e.EmployeeNumber)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.TypeNavigation)
                    .WithMany(p => p.Leave)
                    .HasForeignKey(d => d.Type)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Leave_LeaveType");
            });

            modelBuilder.Entity<LeaveType>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LutExtentionType>(entity =>
            {
                entity.ToTable("LUT_ExtentionType");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.DescriptionAr)
                    .HasColumnName("Description_AR")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<LutWorkItemType>(entity =>
            {
                entity.ToTable("LUT_WorkItemType");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Value).HasMaxLength(50);
            });

            modelBuilder.Entity<MobileUsers>(entity =>
            {
                entity.HasKey(e => e.EmployeeNumber);

                entity.Property(e => e.EmployeeNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fullname)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.ManagerNumber)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password).IsRequired();

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Substitute>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Data).HasColumnType("date");

                entity.Property(e => e.DateFrom)
                    .HasColumnName("Date_From")
                    .HasColumnType("date");

                entity.Property(e => e.DateTo)
                    .HasColumnName("Date_To")
                    .HasColumnType("date");

                entity.Property(e => e.FkCcId)
                    .HasColumnName("FK_CC_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FkClientId)
                    .HasColumnName("FK_Client_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FkContractId)
                    .HasColumnName("FK_Contract_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FkShiftNo)
                    .HasColumnName("FK_Shift_No")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.NewEmpId)
                    .HasColumnName("New_Emp_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.StopEmpId)
                    .HasColumnName("Stop_Emp_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Taskeen>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Contractname)
                    .HasColumnName("contractname")
                    .HasMaxLength(50);

                entity.Property(e => e.Costcentername)
                    .HasColumnName("costcentername")
                    .HasMaxLength(50);

                entity.Property(e => e.Employeename)
                    .HasColumnName("employeename")
                    .HasMaxLength(200);

                entity.Property(e => e.FkContractId).HasColumnName("FK_ContractID");

                entity.Property(e => e.FkEmployeeId)
                    .HasColumnName("FK_EmployeeID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FkErpRegionCode)
                    .HasColumnName("fk_ERP_RegionCode")
                    .HasMaxLength(20);

                entity.Property(e => e.FkItemId).HasColumnName("FK_ItemID");

                entity.Property(e => e.FkShiftId)
                    .HasColumnName("FK_ShiftID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Itemname)
                    .HasColumnName("itemname")
                    .HasMaxLength(50);

                entity.Property(e => e.Shiftname)
                    .HasColumnName("shiftname")
                    .HasMaxLength(50);

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Tax>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.ErpTaxId)
                    .HasColumnName("ERP_TAX_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsDefault).HasColumnName("Is_Default");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.TaxName).HasMaxLength(50);

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FkBranchId).HasColumnName("FK_BranchID");

                entity.Property(e => e.FkUserTypeId).HasColumnName("FK_UserTypeID");

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.HasOne(d => d.FkBranch)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.FkBranchId)
                    .HasConstraintName("FK_Users_Branh");
            });

            modelBuilder.Entity<WorkItem>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FkWorkItemType).HasColumnName("FK_WorkItemType");

                entity.Property(e => e.ItemCode).HasMaxLength(50);

                entity.Property(e => e.ItemName).HasMaxLength(50);

                entity.Property(e => e.ItemPrice).HasColumnType("money");

                entity.Property(e => e.Tstamp)
                    .HasColumnName("TSTAMP")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.FkWorkItemTypeNavigation)
                    .WithMany(p => p.WorkItem)
                    .HasForeignKey(d => d.FkWorkItemType)
                    .HasConstraintName("FK_WorkItem_LUT_WorkItemType");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
