﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class ContractDetailsTmp
    {
        public int Id { get; set; }
        public int? FkWorkItemId { get; set; }
        public decimal? WorkItemPrice { get; set; }
        public int? NumberOfWorkItem { get; set; }
        public decimal? TotalAmount { get; set; }
        public DateTime? StartDt { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? Tstamp { get; set; }
        public string SessionId { get; set; }
        public string CcId { get; set; }
        public string PrjId { get; set; }

        public virtual WorkItem FkWorkItem { get; set; }
    }
}
