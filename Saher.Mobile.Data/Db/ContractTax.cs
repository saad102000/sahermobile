﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class ContractTax
    {
        public int Id { get; set; }
        public int? FkContractId { get; set; }
        public int? FkTaxId { get; set; }
        public string Erptaxid { get; set; }
        public DateTime? Tstamp { get; set; }

        public virtual Contract FkContract { get; set; }
        public virtual Tax FkTax { get; set; }
    }
}
