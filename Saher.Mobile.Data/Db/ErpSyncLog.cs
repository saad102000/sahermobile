﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class ErpSyncLog
    {
        public int Id { get; set; }
        public int? FkInvoiceId { get; set; }
        public string Message { get; set; }
        public DateTime? Tstamp { get; set; }
    }
}
