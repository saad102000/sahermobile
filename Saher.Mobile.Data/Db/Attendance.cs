﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class Attendance
    {
        public long Id { get; set; }
        public long ErpId { get; set; }
        public string EmployeeNumber { get; set; }
        public DateTime AttendanceTime { get; set; }
        public bool IsCheckIn { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string FaceImage { get; set; }
    }
}
