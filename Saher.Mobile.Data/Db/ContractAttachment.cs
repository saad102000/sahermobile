﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class ContractAttachment
    {
        public int Id { get; set; }
        public int? FkContractId { get; set; }
        public string AttachmentUri { get; set; }
        public DateTime? Tstamp { get; set; }

        public virtual Contract FkContract { get; set; }
    }
}
