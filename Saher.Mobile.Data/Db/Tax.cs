﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class Tax
    {
        public Tax()
        {
            ContractTax = new HashSet<ContractTax>();
        }

        public int Id { get; set; }
        public string TaxName { get; set; }
        public double? TaxValue { get; set; }
        public bool? IsDefault { get; set; }
        public string ErpTaxId { get; set; }
        public DateTime? Tstamp { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? NeedSync { get; set; }

        public virtual ICollection<ContractTax> ContractTax { get; set; }
    }
}
