﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class ClientRegion
    {
        public int Id { get; set; }
        public string ErpClientCode { get; set; }
        public string ErpClientName { get; set; }
        public string ErpRegionCode { get; set; }
        public string ErpRegionName { get; set; }
        public DateTime? Tstamp { get; set; }
        public string ErpProjectCode { get; set; }
        public string ErpProjectName { get; set; }
        public bool? IsActive { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
    }
}
