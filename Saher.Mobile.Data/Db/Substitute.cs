﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class Substitute
    {
        public int Id { get; set; }
        public string FkShiftNo { get; set; }
        public DateTime? Data { get; set; }
        public string FkClientId { get; set; }
        public string FkCcId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string StopEmpId { get; set; }
        public string NewEmpId { get; set; }
        public string FkContractId { get; set; }
    }
}
