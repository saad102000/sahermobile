﻿namespace Saher.Mobile.Data.Db
{
    public partial class UserRoles
    {
        public int EmployeeId { get; set; }
        public string Role { get; set; }
    }
}