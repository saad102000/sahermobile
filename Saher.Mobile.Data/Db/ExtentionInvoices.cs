﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class ExtentionInvoices
    {
        public int Id { get; set; }
        public int? FkExtentionId { get; set; }
        public int? FkInvoiceId { get; set; }
        public int? Type { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? Tstamp { get; set; }
        public decimal? AddedAmount { get; set; }
        public decimal? AmountWithoutTaxt { get; set; }
        public decimal? AddedAmountWithoutTaxt { get; set; }

        public virtual ContractExtention FkExtention { get; set; }
        public virtual Invoice FkInvoice { get; set; }
    }
}
