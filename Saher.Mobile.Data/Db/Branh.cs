﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class Branh
    {
        public Branh()
        {
            Users = new HashSet<Users>();
        }

        public int Id { get; set; }
        public string BranchName { get; set; }
        public DateTime? Tstamp { get; set; }
        public string BranchCode { get; set; }

        public virtual ICollection<Users> Users { get; set; }
    }
}
