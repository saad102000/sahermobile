﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class LeaveType
    {
        public LeaveType()
        {
            Leave = new HashSet<Leave>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Leave> Leave { get; set; }
    }
}
