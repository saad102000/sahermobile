﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class Leave
    {
        public long Id { get; set; }
        public string EmployeeNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Type { get; set; }
        public bool? IsApproved { get; set; }

        public virtual LeaveType TypeNavigation { get; set; }
    }
}
