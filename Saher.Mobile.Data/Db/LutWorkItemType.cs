﻿using System;
using System.Collections.Generic;

namespace Saher.Mobile.Data.Db
{
    public partial class LutWorkItemType
    {
        public LutWorkItemType()
        {
            WorkItem = new HashSet<WorkItem>();
        }

        public int Id { get; set; }
        public string Value { get; set; }

        public virtual ICollection<WorkItem> WorkItem { get; set; }
    }
}
